import java.util.Random;
import java.util.Scanner;

public class Hangman {

    public static class Main
    {
        public static void main(String[] args) {
            System.out.println("H A N G M A N");
            Random rand = new Random();
            Scanner sc = new Scanner(System.in);
            while(true){

                int n = rand.nextInt(10);
                char []arr = new char[n];
                for(int i=0;i<n;i++){
                    arr[i] = (char)('a' +rand.nextInt('z' - 'a'));
                }

                char []res = new char[n];
                for(int i=0;i<n;i++)
                    res[i] = '_';
                char []missed = new char[7];
                int n2 = 0;
                while(n2 < 7){
                    System.out.println("+---+\n|\n|\n|\n===");
                    System.out.print("Missed letters:");
                    for(int i=0;i<n2;i++)
                        System.out.print(missed[i]);
                    System.out.println();

                    for(int i=0;i<n;i++){
                        if(arr[i] == res[i])
                            System.out.print(arr[i]);
                        else
                            System.out.print("_");
                    }

                    System.out.println();
                    int flag = 0;
                    while(true){
                        System.out.println("Guess a letter");
                        char c = sc.next().charAt(0);
                        for(int i=0;i<n;i++){
                            if(c == res[i]){
                                System.out.println("You have already guessed that letter. Choose again.");
                                continue;
                            }
                        }

                        for(int i=0;i<n;i++){
                            if(arr[i] == c){
                                flag = 1;
                                res[i] = c;
                            }}

                        if(flag == 0)
                            missed[n2++] = c;
                        break;
                    }
                    int flag2 = 1;
                    // checking if all the letters are guessed
                    for(int i =0;i<n;i++)
                        if(arr[i]!= res[i]){
                            flag2 = 0;
                            break;
                        }
                    // player won by guessing all the letter
                    if(flag2 == 1){
                        System.out.print("Yes! The secret word is ");
                        for(int i=0;i<n;i++)
                            System.out.print(arr[i]);
                        System.out.println("! You have won!");
                        break;
                    }
                }
                if(n2 == 7){
                    System.out.println("You have loss");
                    break;
                }
                else
                {
                    System.out.println("Do you want to play again? (yes or no)");
                    char ch = sc.next().charAt(0);
                    if(ch == 'n')
                        break;
                }

            }
        }
    }
}
